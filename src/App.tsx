import "./App.css";
import StopBtn from "./component/stopbtn";
import StartBtn from "./component/startbtn";
import ResetBtn from "./component/resetbtn";
import LapBtn from "./component/lapbtn";

import { useEffect, useState } from "react";

function App() {
  const [second, setSecond] = useState<number>(0);
  const [minute, setMinute] = useState<number>(0);
  const [hour, setHour] = useState<number>(0);
  const [check, setCheck] = useState<boolean>(false);
  const [lapArray, setLapArray] = useState<string[]>([]);

  function onStart() {
    setCheck(true);
  }

  function onStop() {
    setCheck(false);
  }

  function onReset() {
    setCheck(false);
    setSecond(0);
    setMinute(0);
    setHour(0);
    setLapArray([]);
  }

  function setLap() {
    if (!lapArray.includes(`${hour}h ${minute}m ${second}s`)) {
      setLapArray([...lapArray, `${hour}h ${minute}m ${second}s`]);
    }
    setCheck(false);
  }

  useEffect(() => {
    let interval: NodeJS.Timer | number | undefined = undefined;
    if (check) {
      interval = setInterval(() => {
        setSecond((prev) => prev + 1);
        if (second === 59) {
          setMinute((prev) => prev + 1);
          setSecond(0);
        }
        if (minute === 59) {
          setHour((prev) => prev + 1);
          setMinute(0);
        }
      }, 1000);
    } else {
      clearInterval(interval);
    }
    return () => {
      if (typeof interval === "number" || typeof interval === "undefined")
        clearInterval(interval);
      else clearInterval(interval);
    };
  });
  return (
    <div className="App">
      <h1>
        {hour}h: {minute}m: {second}s
      </h1>
      <div className="container">
        <StartBtn handleStart={onStart} />
        <ResetBtn handleReset={onReset} />
        <StopBtn handleStop={onStop} />
      </div>
      <LapBtn handleLap={setLap} />

      <div className="LapBox">
        {lapArray.map((ele, index) => {
          return <p key={index}>{ele}</p>;
        })}
      </div>
    </div>
  );
}

export default App;
