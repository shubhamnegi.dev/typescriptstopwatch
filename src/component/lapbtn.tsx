import React from "react";
interface props {
  handleLap: () => void;
}
const LapBtn: React.FC<props> = ({ handleLap }) => {
  return (
    <input
      type="button"
      value="Laps"
      onClick={handleLap}
      style={{ width: "170px", marginTop: "15px" }}
    ></input>
  );
};

export default LapBtn;
