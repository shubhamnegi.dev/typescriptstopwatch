import React from "react";

interface props {
  handleReset: () => void;
}
const ResetBtn: React.FC<props> = ({ handleReset }) => {
  return (
    <input
      type="button"
      value="Reset"
      onClick={handleReset}
      style={{ marginLeft: "10px", marginRight: "10px" }}
    ></input>
  );
};

export default ResetBtn;
