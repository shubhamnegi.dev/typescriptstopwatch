import React from "react";

interface props {
  handleStart: () => void;
}
const StartBtn: React.FC<props> = ({ handleStart }) => {
  return <input type="button" value="Start" onClick={handleStart}></input>;
};

export default StartBtn;
