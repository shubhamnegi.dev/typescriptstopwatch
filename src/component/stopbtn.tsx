import React from "react";
interface props {
  handleStop: () => void;
}
const StopBtn: React.FC<props> = ({ handleStop }) => {
  return <input type="button" value="Stop" onClick={handleStop}></input>;
};

export default StopBtn;
